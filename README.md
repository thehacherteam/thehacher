## Objectif du projet

Nous avons pour projet de livrer une application en utilisant une démarche qualité.

thehatcher est un outil opensource qui permet de proposer :
- *Un ensemble de fonctions de hachage*
- *Un outil de chiffrement AES*
- *Transformation des fichiers (Chiffrement/Déchiffrement)*

## Roles des developpeurs dans l'equipe

- *BESSE YAO* **Lead Developpeur** besseyao@thehatcher.fr 
- *Côme Charles MOUSSOUNDA* **Chef de projet** comecharlesmoussounda@thehatcher.fr 

## Architecture et technologies du projet
- [Diagramme de classe](https://gitlab.com/thehacherteam/thehacher/-/blob/th_come/sources/Diag_Class.vpd.png "Diagramme de classe")
- [Diagramme de cas d'utilisation](https://gitlab.com/thehacherteam/thehacher/-/blob/th_come/sources/Diag_User_CAse.jpg "Diagramme de cas d'utilisation")
- [Diagramme de séquence](https://gitlab.com/thehacherteam/thehacher/-/blob/th_come/sources/diag_seq.png "Diagramme de séquence")

Les technologies utilisées pour la réalisation du projet:
- *Python 3.9.13*
- *Mysqli 8.0.29*
- *Visual Paradigm Studio Online*
- *Balsamiq Mockup 4.5.2*
- *Sonarcloud*


## Points particuliers et risques

Vérification du Done dans notre Trello avant la livraison de l'application
Délai et temps de livraison

Quelques options de notre application:

- [connexion](https://gitlab.com/thehacherteam/thehacher/-/blob/main/sources/mockup/Connexion.png "connexion")
- [Option hashage](https://gitlab.com/thehacherteam/thehacher/-/blob/main/sources/mockup/Hashage.png "Option hashage")
- [Option chiffrement](https://gitlab.com/thehacherteam/thehacher/-/blob/main/sources/mockup/Chiffrer.png "Option chiffrement")


## Définition du ready et du done

Choix et installation de l'environnement de travail:
- *Environnement Gitlab*
- *Visual Studio Code*
- *Teams*
- *Trello*

[Avancé du projet suivant le tableau Trello](https://trello.com/b/2U68bFDy/thehacher "Avancé du projet suivant le tableau Trello")
