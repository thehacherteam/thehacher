class Clé:
    def __init__(self, id, priv, pub):
        self._id = id
        self._priv = priv
        self._pub = pub

    def id(self):
        return self._id
    
    @property
    def priv(self):
        return self._priv

    @property
    def pub(self):
        return self._pub

    @priv.setter
    def priv(self, priv):
        self._priv = priv
        
    @pub.setter
    def pub(self, pub):
        self._pub = pub