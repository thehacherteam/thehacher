class Fonction_de_hachage:
    def __init__(self, id, id_hachage, id_sel, fichier, id_cle):
        self._id = id
        self._id_hachage = id_hachage
        self._id_sel = id_sel
        self._fichier = fichier
        self._id_cle = id_cle

    def id(self):
        return self._id
    
    @property
    def id_hachage(self):
        return self._id_hachage

    @property
    def id_sel(self):
        return self._id_sel

    @property
    def fichier(self):
        return self._fichier

    @property 
    def id_cle(self):
        return self._id_cle

    @id_hachage.setter
    def id_hachage(self, h):
        self._id_hachage = h

    @id_sel.setter
    def id_sel(self, s)
        self._id_sel = s
    
    @fichier.setter
    def fichier(self, f):
        self._fichier = f

    @id_cle.setter
    def id_cle(self, c):
        self._id_cle = c