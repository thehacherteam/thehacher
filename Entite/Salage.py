class Salage:
    def __init__(self, id, value):
        self._id = id
        self._value = value

    def id(self):
        return self._id
    
    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, v):
        self._value = v