class Fonction_de_hachage:
    def __init__(self, id_enregistrement, id_utilisateur):
        self._id_enregistrement = id_enregistrement
        self._id_utilisateur = id_utilisateur
        
    @property
    def id_enregistrement(self):
        return self._id_enregistrement

    @property
    def id_utilisateur(self):
        return self._id_utilisateur
    
    @id_enregistrement.setter
    def id_enregistrement(self, ide):
        self._id_enregistrement = ide

    @id_utilisateur.setter
    def id_utilisateur(self, idu):
        self._id_utilisateur = idu
