import hashlib

class Utilisateur:
    def __init__(self, id, name, login, mot_de_passe):
        self._id = id
        self._name = name
        self._login = login
        self._mot_de_passe = hashlib.new("sha1", mot_de_passe.encode()).hexdigest()

    def id(self):
        return self._id
    
    @property
    def name(self):
        return self._name
    
    @property
    def login(self):
        return self._login

    @property
    def mot_de_passe(self):
        return self._mot_de_passe

    @name.setter
    def name(self, n):
        self._name = n

    @login.setter
    def login(self, l):
        self._login = l

    @mot_de_passe.setter
    def mot_de_passe(self, mdp):
        self._mot_de_passe = hashlib.new("sha1", mdp.encode()).hexdigest()


    