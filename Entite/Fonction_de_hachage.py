class Fonction_de_hachage:
    def __init__(self, id, name):
        self._id = id
        self._name = name

    def id(self):
        return self._id
    
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, n):
        self._name = n